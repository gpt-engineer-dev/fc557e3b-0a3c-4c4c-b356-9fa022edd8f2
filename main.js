document.addEventListener('DOMContentLoaded', () => {
  const addButton = document.getElementById('add-todo');
  const newTodoInput = document.getElementById('new-todo');
  const todoList = document.getElementById('todo-list');

  addButton.addEventListener('click', addTodo);

  function addTodo() {
    const todoText = newTodoInput.value.trim();
    if (todoText === '') return;

    const todoItem = document.createElement('li');
    todoItem.className = 'flex items-center justify-between bg-white px-4 py-2 my-2 shadow-sm';
    todoItem.innerHTML = `
      <span>${todoText}</span>
      <div>
        <button class="complete-todo mr-2 text-green-500">
          <i class="fas fa-check"></i>
        </button>
        <button class="delete-todo text-red-500">
          <i class="fas fa-trash"></i>
        </button>
      </div>
    `;

    const completeButton = todoItem.querySelector('.complete-todo');
    const deleteButton = todoItem.querySelector('.delete-todo');

    completeButton.addEventListener('click', () => {
      todoItem.classList.toggle('line-through');
    });

    deleteButton.addEventListener('click', () => {
      todoList.removeChild(todoItem);
    });

    todoList.appendChild(todoItem);
    newTodoInput.value = '';
  }
});
